/**
 * 
 */
package hu.tanfolyam.spring_boot.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import hu.tanfolyam.spring_boot.entity.User;

/**
 * @author Hápy
 *
 */
public interface UserRepository extends JpaRepository<User, Long>  {
	
	public User findByUsername(String username); 

}
