/**
 * 
 */
package hu.tanfolyam.spring_boot.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import hu.tanfolyam.spring_boot.entity.Role;

/**
 * @author Hápy
 *
 */
public interface RoleRepository extends JpaRepository<Role, String> {
	@Query("SELECT r FROM Role r WHERE r.name = ?1")
	public Role findByName(String name);
}
