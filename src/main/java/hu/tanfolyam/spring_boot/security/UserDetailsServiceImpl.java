/**
 * 
 */
package hu.tanfolyam.spring_boot.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import hu.tanfolyam.spring_boot.dao.UserRepository;
import hu.tanfolyam.spring_boot.entity.User;

/**
 * @author Hápy
 *
 */
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;

    @Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsername(username);
		if (user == null) {
			throw new UsernameNotFoundException("Coult not find user");
		}
		return new MyUserDetails(user);
	}

}
